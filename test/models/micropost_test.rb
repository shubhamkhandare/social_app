require 'test_helper'

class MicropostTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  	def setup
  		@user = users(:micheal)
  		# @micropost = Micropost.new(content: "Lorem ipsuM", user_id: @user.id)
  		@micropost = @user.microposts.build(content: "Lorem ipsuM")	
  	end

  	test  "should be valid" do
  		assert @micropost.valid?
  	end

  	test "user id should be present for micropost" do
  		@micropost.user_id = nil
  		assert_not @micropost.valid?
  	end

  	test "content should be present" do
  		@micropost.content = ""
  		assert_not @micropost.valid?
  	end

  	test "content should not exceed 140 characters" do
  		@micropost.content = "a" *141
  		assert_not @micropost.valid?
  	end

  	test "order should be most recent first" do
		assert_equal microposts(:most_recent), Micropost.first
  	end

end
